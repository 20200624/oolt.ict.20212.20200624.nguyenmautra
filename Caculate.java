import javax.swing.JOptionPane;
public class Caculate {
    public static void main(String[] args){
        String StrNum1, StrNum2;
        StrNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number",JOptionPane.INFORMATION_MESSAGE);
        StrNum2 = JOptionPane.showInputDialog(null,"Please input the second number: ", "Input the second number", JOptionPane.INFORMATION_MESSAGE);
        double num1 = Double.parseDouble(StrNum1);
        double num2 = Double.parseDouble (StrNum2);
        double sum = num1 + num2;
        double diff = num1 - num2;
        double product = num1 * num2;
        
        if(num2 == 0)
        {
            JOptionPane.showMessageDialog(null,"The sum is: " + sum + "\nThe difference is: "+diff + "\nThe product is: " + product+"\nUnable to perform division\n");
        }
        else
        {
            double dir = num1/num2;
            JOptionPane.showMessageDialog(null,"The sum is: " + sum + "\nThe difference is: "+diff + "\nThe product is: " + product+"\nThe quotient is: " + dir +"\n");
        }
        System.exit(0);
    }
}
