import java.util.Scanner;
public class DrawTriangle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner print = new Scanner(System.in);
		int iN;
		do
		{
			System.out.println("Enter 'n' : ");
			iN = print.nextInt();
			if(iN % 2 == 0)
			{
				System.out.println("Please enter the even number!!");
			}
		}while (iN % 2 == 0);
		int i, j;
		for(i = 0; i < iN; i++)
		{
			for(j = 1; j <= 2*iN + 1; j++)
			{
				if(j >= (iN + 1) - i && j <= (iN + 1) + i)
				{
					System.out.print("*");
				}
				else
				{
					System.out.print(" ");
				}
			}
			System.out.print("\n");
		}
	}
}
