import java.util.Scanner;
public class NumberOfDay {

	public static void main(String[] args) {
		try (// TODO Auto-generated method stub
		Scanner number = new Scanner(System.in)) {
			int month, years;
			System.out.print("Enter the month: ");
			month = number.nextInt();
			System.out.print("Enter the years: ");
			years = number.nextInt();
			switch(month)
			{
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				System.out.println("The number of days in " + month + "/" + years + " is: 31");
				break;
			case 2:
				if(years % 4 == 0)
				{
					System.out.println("The number of days in " + month + "/" + years + " is: 29");
				}
				else
				{
					System.out.println("The number of days in " + month + "/" + years + " is: 28");
				}
				break;
			default:
				System.out.println("The number of days in " + month + "/" + years + " is: 30");
				break;
			}
		}
	}

}
