import java.lang.Math;
import javax.swing.JOptionPane;
public class Solve {
    public static void main(String[] args)
    {
        String Choice, strNum1, strNum2,strNum3,strNum4,strNum5,strNum6;
        int choice;
        double a, b, c, d, e, f, x,y, D, D1, D2;
        do {
            JOptionPane.showMessageDialog(null,"1. The first-degree equation (linear equation) with one variable\n2. The system of first-degree equations\n3. The second-degree equation with one variable\n0. Exit\n");
            do {
                Choice = JOptionPane.showInputDialog(null,"Please input your choice: ","Input your choice", JOptionPane.INFORMATION_MESSAGE);
                choice = Integer.parseInt(Choice);
                if(choice < 0 && choice > 3)
                {
                    JOptionPane.showMessageDialog(null, "Please choose one of the functions above!\n");
                }
            } while (choice < 0 && choice > 3);
            switch (choice) {
                case 1:
                strNum1 = JOptionPane.showInputDialog(null,"Please input the first coefficient: ","Input the first coefficient", JOptionPane.INFORMATION_MESSAGE);
                a = Double.parseDouble(strNum1);
                strNum2 = JOptionPane.showInputDialog(null,"Please input the second coefficient: ","Input the second coefficient", JOptionPane.INFORMATION_MESSAGE);
                b = Double.parseDouble(strNum2);
                if(a == 0)
                {
                    if(b==0)
                    {
                        JOptionPane.showMessageDialog(null, "Infinity solution!\n");
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "No solution!\n");
                    }
                }
                else
                {
                    x = -b/a;
                    JOptionPane.showMessageDialog(null, "Ans:\n\tx = " + x + "\n");
                }
                    break;
                case 2:
                strNum1 = JOptionPane.showInputDialog(null,"Please input the first coefficient: ","Input the first coefficient", JOptionPane.INFORMATION_MESSAGE);
                a = Double.parseDouble(strNum1);
                strNum2 = JOptionPane.showInputDialog(null,"Please input the second coefficient: ","Input the second coefficient", JOptionPane.INFORMATION_MESSAGE);
                b = Double.parseDouble(strNum2);
                strNum3 = JOptionPane.showInputDialog(null,"Please input the third coefficient: ","Input the third coefficient", JOptionPane.INFORMATION_MESSAGE);
                c = Double.parseDouble(strNum3);
                strNum4 = JOptionPane.showInputDialog(null,"Please input the fourth coefficient: ","Input the fourth coefficient", JOptionPane.INFORMATION_MESSAGE);
                d = Double.parseDouble(strNum4);
                strNum5 = JOptionPane.showInputDialog(null,"Please input the fifth coefficient: ","Input the fifth coefficient", JOptionPane.INFORMATION_MESSAGE);
                e = Double.parseDouble(strNum5);
                strNum6 = JOptionPane.showInputDialog(null,"Please input the sixth coefficient: ","Input the sixth coefficient", JOptionPane.INFORMATION_MESSAGE);
                f = Double.parseDouble(strNum6);
                D = a*e - d*b;
                D1 = c*e - f*b;
                D2 = a*f - d*c;
                if(D == 0)
                {
                    if(D1 == 0 && D2 == 0)
                    {
                        JOptionPane.showMessageDialog(null, "Infinity solution!\n");
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "No solution!\n");
                    }
                }
                else
                {
                    x = D1 / D;
                    y = D2/ D;
                    JOptionPane.showMessageDialog(null, "Ans:\n\tx1 = " + x + "   x2 = " + y + "\n");
                }
                    break;
                case 3:
                strNum1 = JOptionPane.showInputDialog(null,"Please input the first coefficient: ","Input the first coefficient", JOptionPane.INFORMATION_MESSAGE);
                a = Double.parseDouble(strNum1);
                strNum2 = JOptionPane.showInputDialog(null,"Please input the second coefficient: ","Input the second coefficient", JOptionPane.INFORMATION_MESSAGE);
                b = Double.parseDouble(strNum2);
                strNum3 = JOptionPane.showInputDialog(null,"Please input the third coefficient: ","Input the third coefficient", JOptionPane.INFORMATION_MESSAGE);
                c = Double.parseDouble(strNum3);
                if(a == 0)
                {
                    if(b == 0 && c == 0)
                    {
                        JOptionPane.showMessageDialog(null, "Infinity solution!\n");
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "No solution!\n");
                    }
                }
                else
                {
                    D = b*b - 4*a*c;
                    if(D < 0)
                    {
                        JOptionPane.showMessageDialog(null, "No solution!\n");
                    }
                    if(D == 0)
                    {
                        x = -b/(2*a);
                        JOptionPane.showMessageDialog(null, "Ans:\n\tx1 = x2 = "+ x + "\n");
                    }
                    if(D > 0)
                    {
                        x = (-b - Math.sqrt(D))/(2*a);
                        y = (-b + Math.sqrt(D))/(2*a);
                        JOptionPane.showMessageDialog(null, "Ans:\n\tx1 = " + x + "   x2 = " + y + "\n");
                    }
                }
                    break;
                default:
                JOptionPane.showMessageDialog(null, "Thank for used!\n");
                    break;
            }
        } while (choice != 0);
    }
}
