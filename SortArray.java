import java.util.Scanner;
public class SortArray {

	public static void main(String[] args) {
		try (// TODO Auto-generated method stub
		Scanner array = new Scanner(System.in)) {
			int n;
			System.out.print("Enter the size of array: ");
			n = array.nextInt();
			int Arr[] = new int[n];
			int Sum = 0;
			for(int i = 0; i < n; i++)
			{
				System.out.print("Enter arr["+i+"]: ");
				Arr[i] = array.nextInt();
				Sum = Sum + Arr[i];
			}
			System.out.println("The following array you entered is:");
			for(int i = 0; i < n; i++)
			{
				System.out.println("Arr[" + i + "]: " + Arr[i]);
			}
			int temp;
			for(int i = 0; i < n - 1; i++)
			{
				for(int j = i + 1; j < n; j++)
				{
					if(Arr[i] > Arr[j])
					{
						temp = Arr[i];
						Arr[i] = Arr[j];
						Arr[j] = temp;
					}
				}
			}
			
			System.out.println("The sorted array is: ");
			for(int i = 0; i < n; i++)
			{
				System.out.println("Arr[" + i + "]: " + Arr[i]);
			}
			System.out.println("The sum of array is: " + Sum);
		}
	}

}
