import java.util.Scanner;
public class addTwoMatrix {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner matrix = new Scanner(System.in);
		int m,n;
		System.out.print("Enter the size of two matrix(m*n): ");
		m = matrix.nextInt();
		n = matrix.nextInt();
		int Matrix1[][] = new int [m][n];
		int Matrix2[][] = new int [m][n];
		System.out.print("Enter the first matrix: \n");
		for(int i = 0; i < m; i++)
		{
			for(int j = 0; j < n; j++)
			{
				System.out.print("["+ i + "][" + j + "] : ");
				Matrix1[i][j] = matrix.nextInt();
			}
		}
		System.out.print("Enter the second matrix: \n");
		for(int i = 0; i < m; i++)
		{
			for(int j = 0; j < n; j++)
			{
				System.out.print("["+ i + "][" + j + "] : ");
				Matrix2[i][j] = matrix.nextInt();
			}
		}
		System.out.print("The first matrix: \n");
		for(int i = 0; i < m; i++)
		{
			for(int j = 0; j < n; j++)
			{
				System.out.print(Matrix1[i][j] + " ");
			}
			System.out.print("\n");
		}
		System.out.print("The second matrix: \n");
		for(int i = 0; i < m; i++)
		{
			for(int j = 0; j < n; j++)
			{
				System.out.print(Matrix2[i][j] + " ");
			}
			System.out.print("\n");
		}
	}

}
